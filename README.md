#PyCSP

Molecular crystal structure prediction. Started as an excuse to learn PYTHON so most of the code is in PYTHON. 

## Features
- Specify molecule and partial charges and number of copies as input in gen_init.py and LJ/BM potential parameters. 
- Should optimise the cell to generate global minimum using a combination of rigid-body rotations, translations and quaternion moves with bfgs minimisations along the way (a la basin hopping).
- Tested only for benzene. Parameters hard-coded :-(