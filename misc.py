import math
import os
import numpy as np
from numpy import cos, sin, sqrt
from moves import MV
from elements import ELEMENTS
import random

class misc() :
    #miscellaneous routines needed for random stuff
    def __init__(self):
        self.natoms_m=12

    def Cart_to_frac(self, natom,U_cart,a,b,c,alpha,beta,gamma):
        a2r = 0.0174532925
        alpha = a2r * alpha
        beta  = a2r * beta
        gamma = a2r * gamma
        gamma = a2r * gamma

        # (scaled) volume of the cell
        v = math.sqrt(1 -cos(alpha)*cos(alpha) - cos(beta)*cos(beta) - cos(gamma)*cos(gamma)
                      + 2*cos(alpha)*cos(beta)*cos(gamma))
        tmat = np.matrix([[ 1.0 / a,-cos(gamma)/(a*sin(gamma)), (cos(alpha)*cos(gamma)-cos(beta)) / (a*v*sin(gamma))  ],
                          [ 0.0,     1.0 / b*sin(gamma),         (cos(beta) *cos(gamma)-cos(alpha))/ (b*v*sin(gamma))  ],
                          [ 0.0,     0.0,                        sin(gamma) / (c*v)                                    ] ]
        )
        U_cart = np.array(U_cart)
        U_cart=np.reshape(U_cart,[natom,3])
        #        print U_cart
        U_frac = U_cart * tmat.T
        U_frac = np.array(U_frac)
        #print U_frac
        return U_frac

    def Centerofmass(self,U_m):
        #print   "in com:"
        ##assuming equal qeights,
        Ucom_m = 0
        Ucom_m_x,Ucom_m_y,Ucom_m_z = 0,0,0
        Ucom_m += U_m.sum(0) / len(U_m)
        #print Ucom_m

        return Ucom_m


    def COM_all(self,U,natoms_m):
        nmol = len(U)/natoms_m
        #print nmol
        U_com = []
        for o in range(1,nmol+1):
            U_mol = []
            Ucom_m = 0
            for i in range(o*natoms_m-natoms_m,o*(natoms_m)) :
                U_mol.append(np.array(U[i,:]))
            U_mol = np.array(U_mol, dtype=float)
            Ucom_m += U_mol.sum(0) / len(U_mol)
            U_com.append(np.array(Ucom_m))
            #print U_com
        #print "Center of rotation for", len(U_com), "molecules in U_com"
        return U_com
    def cell_to_vec(self,cell):
        #1)generate box vectors from a,b,c,alp,bet,gam
        natoms_m = 1
        a = np.float64(cell[0])
        b = np.float64(cell[1])
        c = np.float64(cell[2])
        alpha = np.float64(cell[3])
        beta = np.float64(cell[4])
        gamma = np.float64(cell[5])
        a2r = 0.0174532925
        alpha = a2r * alpha
        beta  = a2r * beta
        gamma = a2r * gamma
        nmol = len(U)/natoms_m
        #scaled volume of cell
        v = math.sqrt(1 -cos(alpha)*cos(alpha) - cos(beta)*cos(beta) - cos(gamma)*cos(gamma)
                      + 2*cos(alpha)*cos(beta)*cos(gamma))
        #cell vectors in x y and z.
        v1 = [1.0 / a, -cos(gamma)/(a*sin(gamma)), (cos(alpha)*cos(gamma)-cos(beta)) / (a*v*sin(gamma))]
        v2 =  [ 0.0, 1.0 / b*sin(gamma),(cos(beta) *cos(gamma)-cos(alpha))/ (b*v*sin(gamma)) ]
        v3 = [ 0.0,0.0, sin(gamma) / (c*v) ]
        #move to fractionals, all Rj
        return

    def calc_dist_matrix(self,U,cell):
        #calculates distances between all atoms
        npart = 2
        r = []
        mv = MV()
        natom = len(U)
        for i in range(npart):
            for j in range(npart):
                if i > j:
                    X  = U[j,0] - U[i,0]
                    Y  = U[j,1] - U[i,1]
                    Z  = U[j,2] - U[i,2]
                    # Distance squared
                    r2 = X*X + Y*Y + Z*Z
                    r = math.sqrt(r2)
        return r


    def write_xyz(self,filename,U_cart,nmol,atomnames,cell,E):
        #natom_m = number of atoms in molecule.
        #natoms = total number of atoms in the system.
        natom_m = 1
        print natom_m,atomnames

        natoms = len(U_cart)
        f = open (filename,'a')
        f.write('%d \n'%(natoms))
        f.write('%f %s \n'%(E,cell))
        for i in range(0,natoms) :
            #print i%natom_m
            atom= atomnames[i % natom_m]
            f.write('%s\t %2.3f\t%2.3f\t%2.3f \n'%(atom,U_cart[i,0],U_cart[i,1],U_cart[i,2]))
        f.close()
        return

    def split_U(self, U, natom):
        for i in xrange(0, len(l), n):
            yield l[i:i+n]

    def clean_files(self):
        os.system("rm energies.dat")
        #os.system("rm translate.xyz")
        #os.system("rm rotate.xyz")
        #os.system("rm box.xyz")
        os.system("rm images.xyz")
        os.system("rm unit_cell.xyz")




    def accept_reject(self,Enew,Eold,temperature,random):
        temp = 40
        if Enew < Eold: return True
        acceptstep = True
        wcomp = (Enew - Eold) / temperature
        w = min(1.0, np.exp(-wcomp))
        rand = random
        if rand > w: acceptstep = False

        return acceptstep


    def pack_frac(self,vatom,vcell,natom):
        ele = ELEMENTS['C']
        print "atomic radius : ", ele.atmrad, "A"
        vatom = ele.atmrad*10
        #sigma = 1
        #vatom = 15.625*sigma**3 #LJ atom
        pack = vatom*natom/vcell
        #print "packing fraction", pack
        #molecular radius of crystal:Gibson and Scheraga 1995

        return pack
    def recip_cell(self, l1,l2,l3):
        #returns reciprocal lattice vectors for cell vectors l1,l2,l3:
        pi = math.pi
        cell = np.matrix([
                             [l1[0],l1[1],l1[2]],
                             [l2[0],l2[1],l2[2]],
                             [l3[0],l3[1],l3[2]]
                         ],dtype = float)
        cross = np.cross(l2,l3)
        vol = np.dot(l1,cross)
        rcell = np.empty_like(cell,dtype = float)
        a = cell[0,:]
        b = cell[1,:]
        c = cell[2,:]
        #checke 2*pi/vol factor
        rcell[0,:] = 2*pi/vol * np.cross(b,c)
        rcell[1,:] = 2*pi/vol * np.cross(c,a)
        rcell[2,:] = 2*pi/vol * np.cross(a,b)
        return rcell

    def williams_77(self,atomi,atomj):
        if atomi == 'H' :
            A1 = 136.4
            B1 = 11971
            C1 = 3.74
        if atomi == 'C' :
            A1 = 2439.8
            B1 = 369743
            C1 = 3.60
        if atomi == 'N' :
            A1 = 27.3
            B1 = 2654.0
            C1 = 3.74
        if atomj == 'H' :
            A2 = 136.4
            B2 = 11971
            C2 = 3.74
        if atomj == 'C' :
            A2 = 2439.8
            B2 = 369743
            C2 = 3.60
        if atomj == 'N' :
            A2 = 27.3
            B2 = 2654.0
            C2 = 3.74

        parameters=np.array([A1,B1,C1,A2,B2,C2],dtype = float)
        return parameters






def main():

    natoms = 20
    U = np.random.uniform(-10,10, natoms * 3)
    U = np.reshape(U, [natoms, 3])
    mc=misc()
    atomnames=['C', 'C', 'C', 'C', 'C', 'C', 'H', 'H', 'H', 'H', 'H', 'H']
    cell = [10, 10, 10, 90, 90, 90]
    l1 = [10,0,0]
    l2 = [0,10,0]
    l3 = [0, 0, 10]
    p=mc.write_xyz('test',U,5,atomnames,cell,0.0)



if __name__ == "__main__":
    main()



