import scipy.optimize as spo
import numpy as np
minimizer_kwargs = {"method":"BFGS"}

x = [10]
r6i = x
eps = 1

f1=lambda x: 4.*eps*(1/x**6*(1/x**6 -1))
def mybounds(**kwargs):
    x = kwargs["x_new"]
    tmax = bool(np.all(x <= 1.0))
    tmin = bool(np.all(x >= 0.0))
    print x
    print tmin and tmax
    return tmax and tmin


def print_fun(x, f, accepted):
      print("at minima %.4f accepted %d" % (f, int(accepted)))
x0=[1.]
spo.basinhopping(f1,x0,accept_test=mybounds,callback=print_fun,niter=200,minimizer_kwargs=minimizer_kwargs)
