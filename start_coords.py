import numpy as np
from numpy import cos, sin, sqrt
from elements import ELEMENTS
import misc


__author__ = 'sridhar'
class get_init():
    def read_input(self,file):
        file = input.dat
        f = open(file ,"r")
        lines = iter(f)
        for line in lines:
            line=line.strip()
            line = line.split()
            molfile = float(line[1])
            seed = int(line[1])
            print molfile
            #print seed


    def readmol(self,file1) :
        #read from file
        #_m => variable for molecule
        #natom_m = number of atoms in molecule
        #type_m[][] = atom type,molecule type
        #natom_type[0:type_m] = numbe of atoms of type 1,2,..type_m
        #mtype,atype are used to assign interaction types

        global natom_m,type_m,name_m,U_m,mass_m,ntype,atype,charge_m
        U_m = []
        type_m = []
        name_m=[]
        mass_m = []
        charge_m = []
        f = open(file1 ,"r")
        lines = iter(f)
        for line in lines:
            line = line.strip()
            line = line.split()
            natom_m = int(line[0])
            Ux_m=float(line[3])
            Uy_m=float(line[4])
            Uz_m=float(line[5])
            #           name_m=string(line[1])
            U_m.append(np.array([Ux_m,Uy_m,Uz_m]))
            type_m.append(int(line[2]))
            name_m.append(line[1])
            charge_m.append(float(line[6]))
        U_m=np.array(U_m)
        type_m=np.array(type_m)
        #find different types of atoms in molecule
        atype=np.unique(name_m)
        ntype=np.unique(type_m)
        n_inter=(len(atype)*(len(atype)-1)/2) + len(atype)
        #print "--------------",file,"----------------------"
        # print "Number of atoms  = ",natom_m
        # print "Types of atoms  = ", type_m
        # print "Names of atoms   = ", name_m
        # print "Number of interactions including self in molecule =", n_inter
        # print "Charge on atoms:", charge_m

        for i in range(1,natom_m):
            m=ELEMENTS[name_m[i]].mass
            mass_m.append(m)
        #print "Atomic mass of atoms =", mass_m
        f.close()

        #f1 = open("cell.dat" ,"r")
        #lines = iter(f1)

        #for line in lines:
       #     line = line.strip()
       #     line = line.split()
       #     l1 = float(line[0])
       #     print l1









        commons=[natom_m,type_m,name_m,U_m,mass_m,ntype,atype,charge_m]





        return  commons


    def gen_init(self,moltype,nmol,a,b,c,alpha,beta,gamma,seed) :
        if moltype >  1:
            "Sorry. Only one type so far"
            exit()
        np.random.seed(seed)
        a2r = 0.0174532925
        natom = natom_m*nmol
        global U_cart,charge,atomlist
        #print "Box dimensions",a,b,c
        Ucom_m = 0
        #       print U_m
        Ucom_m += U_m.sum(0) / len(U_m)
        Ucom_m = np.array(Ucom_m)
        Uref_m = U_m - Ucom_m
        Uref_m = np.array(Uref_m)
        # make copies of molecule. same molecule at different distances
        #center of box is origin.
        U_cart = []
        print "Making", nmol, "copies"
        atomlist = []
        charge = []
        # expand atomlist and charge to all molecules:
        for i in range(0,natom):
            atomlist.append(name_m[i%natom_m])
            # print atomlist[i]
            charge.append(charge_m[i%natom_m])
            #print atomlist
        #        print "len(atomlist)",len(atomlist)
        #       print "len(charge)", len(charge)


        for i in range (1,nmol+1) :
            xa = np.random.uniform(0,a)
            xb = np.random.uniform(0,b)
            xc = np.random.uniform(0,c)
            U_cart.append((Uref_m + [xa,xb,xc]))


        U_cart = np.array(U_cart)
        U_cart=np.reshape(U_cart,[natom,3])
        #print "len(U_cart)",len(U_cart)


        #print U_fracPim = np.array(Pim)

        #U is the array of shape (nmol,natom_mol,3) with all coordinates of all atoms
        #       print "Total number of atoms", U_Cart.size/3

        #      GENERATE ATOM LIST FOR IDENTIFYING ATOMNAMES AND NUMBERS

        #U_tag should be array with the following data
        # U_tag = [moltype,molnumber,atomtype,atomnumber,x,y,z]
        # keep track of inter molecular interactions
        #assign interactions:
        return U_cart


