import random
import math
from numpy import sin,cos,tan,sqrt,e
import numpy as np
from potentials import LJ


__author__ = 'sridhar'
class MV() :
# read eps and sig from file
    def __init__(self,mtype = 1,a=10,b=10,c=10,alpha=90,beta=90,gamma=90):
        self.mtype =  mtype

    def Cart_to_frac(self,U_cart,cell):
                    a = np.float64(cell[0])
                    b = np.float64(cell[1])
                    c = np.float64(cell[2])
                    alpha = np.float64(cell[3])
                    beta = np.float64(cell[4])
                    gamma = np.float64(cell[5])
                    a2r = 0.0174532925
                    alpha = a2r * alpha
                    beta  = a2r * beta
                    gamma = a2r * gamma
                    v = sqrt(1 -cos(alpha)*cos(alpha) - cos(beta)*cos(beta) - cos(gamma)*cos(gamma) + 2*cos(alpha)*cos(beta)*cos(gamma))

                    tmat = np.matrix( [
                    [ 1.0 / a, -cos(gamma)/(a*sin(gamma)), (cos(alpha)*cos(gamma)-cos(beta)) / (a*v*sin(gamma))  ],
                    [ 0.0,     1.0 / b*sin(gamma),         (cos(beta) *cos(gamma)-cos(alpha))/ (b*v*sin(gamma))  ],
                    [ 0.0,     0.0,                        sin(gamma) / (c*v)                                    ] ]
                    )

#                    tmat = np.matrix( [[ 1.0 / a, 0.0 , 0.0  ],
#                                       [ 0.0, 1.0 / b, 0 ],
#                                       [ 0.0, 0.0, 1/c] ])
                    #U_cart=np.reshape(U_cart,[natom,3])
                    #        print U_cart
                    U_frac = U_cart * tmat.T
                    U_frac = np.array(U_frac,dtype=float)
                    #print U_frac
                    return U_frac

    def Frac_to_cart(self,natom,U_frac,cell):
                    a = cell[0]
                    b = cell[1]
                    c = cell[2]
                    alpha = cell[3]
                    beta = cell[4]
                    gamma = cell[5]
                    a2r = 0.0174532925
                    alpha = a2r * alpha
                    beta  = a2r * beta
                    gamma = a2r * gamma

                # (scaled) volume of the cell
                    v = math.sqrt(1 -cos(alpha)*cos(alpha) - cos(beta)*cos(beta) - cos(gamma)*cos(gamma)
                     + 2*cos(alpha)*cos(beta)*cos(gamma))
                    tmat = np.matrix([[a, 0 ,0  ], [0,b,0],[ 0.0,0,c] ] )
                    U_frac = np.array(U_frac)
#
                    #        print U_cart
                    U_cart = U_frac * tmat
                    U_cart = np.array(U_cart)
                    #print U_cart
                    return U_cart


    def Pick_mol(self,nmol,U_cart):
        global o,natoms_m,n1,n2
        natoms_m = 2
        o = np.random.randint(1,nmol+1)
        #print o
        #backup old coordlist
        U_old_cart = np.copy(U_cart)
        U_mol = []
        np.random.seed(seed=None)
        n1 = o*natoms_m-natoms_m
        n2 =  o*(natoms_m)-1
        #print "picked mol", o
        #get coordinates of molecule
        #remember index 0 is atom 1 in molecule
        for i in range(o*natoms_m-natoms_m,o*(natoms_m)) :
            U_mol.append(np.array(U_cart[i,:]))
            #print i
            #print "atom1", o*natoms_m-natoms_m, "atomn", o*(natoms_m)-1


        U_mol=np.array(U_mol)


        return U_mol

    def Update_mol_coords(self,U_mol,U_cart,o,natoms_m):
        #use for updating coordinate list after each molecule move
        print "updating coords for molecule", o
        #print "U_mol", len(U_mol)
        #n1 and n2 are global variables set while picking molecule

        for i in range(n1,n2+1) :
                 U_cart[i,:] = U_mol[i-n1,:]
         #        print i
                 #print U_cart[i,:]
        #print "U_cart", len(U_cart)

        return U_cart
    def update_coords_cubiccell(self,U_Cart,ao,an):
    #Be careful. scale only distances between molecules. not molecules themselves!
        factor = ao/an
        U = np.multiply(U_Cart,factor)
        U = np.array(U, dtype = float)
        return U

    def translate_mol(self,U_cart,nmol,step):
        #print 'in translate '

        natoms_m = 2
        o = np.random.randint(1,nmol+1)
        #print o

        U_mol = []
        np.random.seed(seed=None)
        dx =step
        #dx = step
        #print "picked mol", o, "translated by ", dx
        #get coordinates of molecule
        #remember index 0 is atom 1 in molecule

        #read in as global variable????
        #think smarter way for co-crystals
#        misc = misc()
#        U_mol = misc.Pick_mol(U_cart)
        #!!update U_cart here!!
        #print "U_f",U_frac[2],"U_c", U_cart[2],cell,o

        for i in range(o*natoms_m-natoms_m,o*(natoms_m)) :
            #translate molecule in x,y and z inside box(fractionals)
            #only atoms within box are moved
            #for moving atoms outside box, move them back into box?
            U_cart[i,0] = U_cart[i,0] + dx
            U_cart[i,1] = U_cart[i,1] + dx
            U_cart[i,2] = U_cart[i,2] + dx
            #U_mol.append(np.array(U_cart[i,:]))




        #print U_frac[2],U_cart[2],cell,o
        #U_cart = MV.Frac_to_cart(self,natom,U_frac,cell)
        #print U_cart
        #self.misc=misc()
        #U_com=self.misc.Centerofmass(U_mol)
        #rescale coordinates:
        #print U_mol
        #translate U_mol
        return U_cart

    def rotate_mol(self,U_cart,nmol):
        #chose molecule at random. random point and unit vector.

        U_mol=MV.Pick_mol(self,nmol,U_cart)
        #choose angle at random
        Ucom_m += U_mol.sum(0) / len(U_mol)
        angle = (random.random() - 0.5) * (2*math.pi)
        #chose direction at random
        direction = np.random.random(3) - 0.5
        self.misc=misc()
        #chose point at random
        #point =  np.random.random(3) - 0.5
        point= Ucom_m
        sina = math.sin(angle)
        cosa = math.cos(angle)
        #rotation matrix around unit vector
        R = np.diag([cosa,cosa,cosa])
        R += np.outer(direction, direction) * (1.0 - cosa)
        direction *= sina
        R += np.array([[ 0.0,         -direction[2],  direction[1]],
                      [ direction[2], 0.0,          -direction[0]],
                      [-direction[1], direction[0],  0.0]])
        M = np.identity(4)
        M[:3, :3] = R
        #M is matrix to rotate about axis defined by point and direction
        if point is not None:
            # rotation not around origin
            point = np.array(point[:3], dtype=np.float64, copy=False)
            M[:3, 3] = point - np.dot(R, point)
            U_rot = U_mol - np.dot(R,point)
            #print "U_rot", U_rot
#       update list
            U_cart = MV.Update_mol_coords(self,U_rot,U_cart,o,natoms_m)


        return U_cart


    def rotate_mol_euler(self,U_cart,nmol):
        #http://nbviewer.ipython.org/github/demotu/BMC/blob/master/notebooks/Transformation3D.ipynb
        #print "in rotate euler"
        natom_m = 2
        U_mol = self.Pick_mol(nmol,U_cart)
        #rotation matrices
        # Elemental rotation matrices of xyz in relation to XYZ:
        #alpha= rotation along x-axis, beta=rotation along y-axis, gamme=rot along z-axis
        #alpha= rotation along x-axis, beta=rotation along y-axis, gamme=rot along z-axis
        alpha = (random.random() - 0.5) * (2*math.pi)
        beta  = (random.random() - 0.5) * (2*math.pi)
        gamma  = (random.random() - 0.5) * (2*math.pi)
        sina = math.sin(alpha)
        sinb = math.sin(beta)
        sing = math.sin(gamma)
        cosa = math.cos(alpha)
        cosb = math.cos(beta)
        cosg = math.cos(gamma)
        RX = np.matrix([[1, 0, 0], [0, cosa, -sina], [0, sina, cosa]])
        RY = np.matrix([[cosb, 0, sinb], [0, 1, 0], [-sinb, 0, cosb]])
        RZ = np.matrix([[cosg, -sing, 0], [sing, cosg, 0], [0, 0, 1]])
        # Rotation matrix of xyz in relation to XYZ:
        RXYZ = RZ*RY*RX
        U_euler = U_mol*RXYZ
        #update U_cart
        U_cart = MV.Update_mol_coords(self,U_euler,U_cart,o,natoms_m)
        #calculate energy

        return U_cart

    def scale_box(self,U,cell):
        a = np.float64(cell[0])
        b = np.float64(cell[1])
        c = np.float64(cell[2])
        alpha = np.float64(cell[3])
        beta = np.float64(cell[4])
        gamma = np.float64(cell[5])
        a2r = 0.0174532925
        alpha = a2r * alpha
        beta  = a2r * beta
        gamma = a2r * gamma
        pi = math.pi
        natoms_m = 2 #add read
        vmove = 0.01*a #scales down slowly increasing the pressure.
        a_move = 0.01*a #20%variation in a, b and c
        #andom.seed(10)
        v = 0
        v=math.sqrt(1 -cos(alpha)*cos(alpha) - cos(beta)*cos(beta) - cos(gamma)*cos(gamma)
                    + 2*cos(alpha)*cos(beta)*cos(gamma))
        #  cell vectors:
        l1 = (a,b*cos(gamma),c*cos(beta))
        l2 = (0,b*sin(gamma),c*(cos(alpha) - (cos(beta)*cos(gamma))/sin(gamma)))
        l3 = (0, 0,c*v/sin(gamma))
        #cross = np.cross(l2,l3)
        #vo = np.dot(l1,cross)
        vo= a*b*c*(math.sqrt(1 -cos(alpha)*cos(alpha) - cos(beta)*cos(beta) - cos(gamma)*cos(gamma)
                    + 2*cos(alpha)*cos(beta)*cos(gamma)))
        ao = math.pow(vo,1/3.0)
#random walk in logv
        #cell_move = 1 => walk in log volume space,cubic cell,increase pressure slowly
        #cell_move = 2 => random displacement of a,b and c. orthorhombic cell.
        #cell_move = 3 => floppy box
        cell_move = 2
        if cell_move==1 :
                lnvn = math.log( vo ) - (random.random())*vmove
                vn = math.exp(lnvn)
                print "ao = ", ao
                an = math.pow(vn,1/3.0)
                a = an
                b = an
                c = an
                print "an = ", an
                alpha = 90
                beta = 90
                gamma = 90
                cell = [a,b,c,alpha,beta,gamma]
                #update coordinates, if step accepted:

                U_new = self.update_coords_cubiccell(U,ao,an)
                print U_new


                #print "cell_move set to 1 so box is made cubic based on volume"
                #print "V_n =", vn
#new cell vectors to reproduce new volume?
        if cell_move == 2 :
        #random displacement of a, b and c. angles remain pi/2
               # print "cell move = 2, cell remains orthorhombic"
                a = a + (random.random() - 0.5)*a_move*a
                b = b + (random.random() - 0.5)*a_move*b
                c = c + (random.random() - 0.5)*a_move*c
                alpha = 90
                beta = 90
                gamma = 90
                cell = np.float64([a,b,c,alpha,beta,gamma])
        if cell_move == 3:
         natoms=len(U)
         mc=misc()
         lj = LJ()
         U_com = mc.COM_all(U,natoms_m)
         cell_new = self.scale_box(U,cell)
         print "new cell parameters", cell_new,cell
         #check if COM lies within simulation cell.
        #3) Each particle has outscribed sphere of radius R0 with center at Rj. R0-= cut-off radius?


        return cell


    def scale_box_new(self,cell,step) :
        i = np.random.random()
        print "SCALE BOX", i
        #print cell
        step = 1
        cell_step = step


        #orthorhombic = False
        orthorhombic = True
        if (orthorhombic):
            if i < 0.33:
                cell[0,0] = np.array(cell[0,0] + cell_step*((random.random() - 0.5)))
            if i > 0.33 and i < 0.66:
                cell[1,1] = np.array(cell[1,1] + cell_step*((random.random() - 0.5)))
            if i > 0.66  :
                cell[2,2] = np.array(cell[2,2] + cell_step*((random.random() - 0.5)))
        else :

            if i < 0.33:
                j = random.random()
                if j <0.5 :
                    cell[0,0] = np.array(cell[0,0]) - cell_step
                elif j <0.75 and j > 0.5:
                    cell[0,1] = np.array(cell[0,1]) + cell_step
                else :
                    cell[0,2] = np.array(cell[0,2]) + cell_step

            if i > 0.33 and i < 0.66:
                j = random.random()
                if j <0.25 :
                    cell[1,0] = np.array(cell[1,0]) + cell_step
                elif j <1 and j > 0.5:
                    cell[1,1] = np.array(cell[1,1]) - cell_step
                else :
                    cell[1,2] = np.array(cell[1,2]) + cell_step
            if i > 0.66  :
                j = random.random()
                if j <0.25 :
                    cell[2,0] = np.array(cell[2,0]) + cell_step
                elif j <0.5 and j > 0.25:
                    cell[2,1] = np.array(cell[2,1]) + cell_step
                else :
                    cell[2,2] = np.array(cell[2,2]) - cell_step

        #print cell



        return cell

def main():
    natoms = 2
    nmol = 1
    U = np.random.uniform(-10, 10, natoms * 3)
    U = np.reshape(U, [natoms, 3])
    mv = MV()



    H = np.matrix([
            [10, 0, 0],
            [0, 10, 0],
            [0, 0, 10]
        ],dtype = float)
    l1 = H[0]
    cell = H
    #cell_new = mv.scale_box_new(cell)

    E1 = mv.scale_box_new(cell,0.1)
    print E1
    #print U
    #U_new = mv.translate_mol(U,nmol,0.2)
    #print U_new




if __name__ == "__main__":
    main()


