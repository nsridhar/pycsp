import numpy as np
from numpy import cos, sin, sqrt
from potentials import LJ
from moves import MV
from misc import misc
from misc import Metropolis
from fl_box import floppy_box
from elements import ELEMENTS

##Read data into variables from one file

#_m => variable for one molecule
#natom_m = number of atoms in molecule
#type_m = atom type
#natom_type[0:type_m] = numbe of atoms of type 1,2,..type_m
#



def readmol(file) :
#read from file
#_m => variable for molecule
#natom_m = number of atoms in molecule
#type_m[][] = atom type,molecule type
#natom_type[0:type_m] = numbe of atoms of type 1,2,..type_m
#mtype,atype are used to assign interaction types

        global natom_m,type_m,name_m,U_m,mass_m,ntype,atype,charge_m
        U_m = []
        type_m = []
        name_m=[]
        mass_m = []
        charge_m = []
        f = open(file ,"r")
        lines = iter(f)
        for line in lines:
            line = line.strip()
            line = line.split()
            natom_m = int(line[0])
            Ux_m=float(line[3])
            Uy_m=float(line[4])
            Uz_m=float(line[5])
#           name_m=string(line[1])
            U_m.append(np.array([Ux_m,Uy_m,Uz_m]))
            type_m.append(int(line[2]))
            name_m.append(line[1])
            charge_m.append(float(line[6]))
        U_m=np.array(U_m)
        type_m=np.array(type_m)
        #find different types of atoms in molecule
        atype=np.unique(name_m)
        ntype=np.unique(type_m)
        n_inter=(len(atype)*(len(atype)-1)/2) + len(atype)
        print "--------------",file,"----------------------"
        print "Number of atoms  = ",natom_m
        print "Types of atoms  = ", type_m
        print "Names of atoms   = ", name_m
        print "Number of interactions including self in molecule =", n_inter
        print "Charge on atoms:", charge_m

        for i in range(1,natom_m):
            m=ELEMENTS[name_m[i]].mass
            mass_m.append(m)
        print "Atomic mass of atoms =", mass_m

        commons=[natom_m,type_m,name_m,U_m,mass_m,ntype,atype,charge_m]

        return  commons


def gen_init(moltype,nmol,a,b,c,alpha,beta,gamma,seed) :
        np.random.seed(seed)
        a2r = 0.0174532925
        natom = natom_m*nmol
        global U_cart,charge,atomlist
        print "Box dimensions",a,b,c
        Ucom_m = 0
#       print U_m
        Ucom_m += U_m.sum(0) / len(U_m)
        Ucom_m = np.array(Ucom_m)
        Uref_m = U_m - Ucom_m
        Uref_m = np.array(Uref_m)
# make copies of molecule. same molecule at different distances
        #center of box is origin.
        U_cart = []
        print "Making", nmol, "copies"
        atomlist = []
        charge = []
# expand atomlist and charge to all molecules:
        for i in range(0,natom):
            atomlist.append(name_m[i%natom_m])
           # print atomlist[i]
            charge.append(charge_m[i%natom_m])
             #print atomlist
        print "len(atomlist)",len(atomlist)
        print "len(charge)", len(charge)


        for i in range (1,nmol+1) :
              xa = np.random.uniform(-a/2,a/2)
              xb = np.random.uniform(-b/2,b/2)
              xc = np.random.uniform(-c/2,c/2)
              U_cart.append((Uref_m + [xa,xb,xc]))

         # convert to radians

        alpha = a2r * alpha
        beta  = a2r * beta
        gamma = a2r * gamma
        gamma = a2r * gamma

    # (scaled) volume of the cell

        v = sqrt(1 -cos(alpha)*cos(alpha) - cos(beta)*cos(beta) - cos(gamma)*cos(gamma)
                 + 2*cos(alpha)*cos(beta)*cos(gamma))

        tmat = np.matrix([[ 1.0 / a,-cos(gamma)/(a*sin(gamma)), (cos(alpha)*cos(gamma)-cos(beta)) / (a*v*sin(gamma))  ],
                [ 0.0,     1.0 / b*sin(gamma),         (cos(beta) *cos(gamma)-cos(alpha))/ (b*v*sin(gamma))  ],
                [ 0.0,     0.0,                        sin(gamma) / (c*v)                                    ] ]
                )

        U_cart = np.array(U_cart)
        U_cart=np.reshape(U_cart,[natom,3])
        print "len(U_cart)",len(U_cart)

        U_frac = U_cart * tmat.T
        U_frac = np.array(U_frac)

        #print U_frac

#U is the array of shape (nmol,natom_mol,3) with all coordinates of all atoms
#       print "Total number of atoms", U_Cart.size/3

#      GENERATE ATOM LIST FOR IDENTIFYING ATOMNAMES AND NUMBERS

#U_tag should be array with the following data
# U_tag = [moltype,molnumber,atomtype,atomnumber,x,y,z]
# keep track of inter molecular interactions
        #assign interactions:
        return U_cart

def run_moves(nsteps,U_cart,nmol,cell,rcut):
    #make this better. assign percentage of moves in input.

            global E_new,count_rot,count_trans, count_box
            lj = LJ()
            mv = MV()
            mc = misc()
            temperature = 1
            met = Metropolis(temperature,np.random.rand)
            #E_old = lj.lennardjones(U_cart,cell,rcut)
            bmpluscoul=1

            if bmpluscoul==1:
                E_old = lj.bm(U_cart,cell,atomlist,rcut,charge)
                E_initial = E_old
            # testing with stupid moves: works?? check final U_cart
            # add interaction parser.(done)
            # add strategy for optimisation. (ntrans+nrot+nquat+nboxrepeat on some prob distribution)
                nsteps_trans = 0
                nsteps_rot = 4
                nsteps_box = 0
                f = open ('energies.dat','a')
                #ROTATE
                print "IN ROT STEP:", nsteps_rot, "steps"
                count = 0
                for i in range(1,nsteps_rot+1) :
                    U_cart_rot = mv.rotate_mol(U_cart,nmol)
                    #E_new = lj.lennardjones(U_cart,cell,rcut)
                    E_new = lj.bm(U_cart,cell,atomlist,rcut,charge)
                    #accept = met.acceptRejectE(E_old,E_new)
                    #print accept
                    if (E_new < E_old):
                    #metropolis. see misc.metropolis
                        E_old=E_new
                        count = count + 1
                        U_cart = U_cart_rot
                        print "r_Step:",i,E_new
                        mc.write_xyz('rotate.xyz',U_cart,nmol,name_m,cell,E_new)
                        f.write('%d %2.3f \n'%(i,E_new))
                count_rot=count

                #TRANSLATE
                print "IN TRANS STEP", nsteps_trans, "steps"
                count=0
                for i in range(1,nsteps_trans+1) :
                #print i
                    U_cart_trans = mv.translate_mol(U_cart,nmol,cell)
                    #E_new = lj.lennardjones(U_cart,cell,rcut)
                    E_new = lj.bm(U_cart,cell,atomlist,rcut,charge)
                    #print "in_trans_step:", i,E_new,E_old

                    if (E_new < E_old):
                        E_old=E_new
                        U_cart = U_cart_trans
                        count = count +1
                        f.write('%d %2.3f \n'%(i,E_new))
                        #print "Step:",i, E_new/natom, "eV/atom",count
                        mc.write_xyz('translate.xyz',U_cart,nmol,name_m,cell,E_new)
                count_trans = count

                #BOX MOVES
                print "IN BOX STEP", nsteps_box, "steps"
                count=0
                for i in range(1,0) :
                    cell_new =mv.scale_box(U_cart,cell)
                    print cell_new
                    U_cart_box = mv.scale_box(U_cart,cell_new)
                    E_new = lj.bm(U_cart,cell,atomlist,rcut,charge)
                    mc.write_xyz('box.xyz',U_cart,nmol,name_m,cell,E_new)
                    print "in_box_step:", i,E_new,E_old
                    f.write('%d %2.3f \n'%(i,E_new))
                    #print "Step:",i, E_new/natom, "eV/atom",count
                    mc.write_xyz('box.xyz',U_cart,nmol,name_m,cell,E_new)
                    count_box = count

                print "E_new =", E_new, "E_initial =", E_initial
                outfile = "config.xyz"
                np.savetxt(outfile,U_cart)
                print name_m

            return U_cart








def main() :
#call functions and set initial parameters
	mol1='benzene.dat'
	mol2='hf_benzene.dat'
	readmol(mol1)
	print "1) Reading molecule 1"
#      print E_lj_old, E_lj_new print U_m, U_m.shape
#   other parameters
# for floppy box nmol <= 8 should be ideal.
        nmol1=8
        a = 20
        b = 20
        c = 20
        rcut=10
        alpha = 90
        beta = 90
        gamma = 90
        moltype=1
        seed = 123456
        natoms = nmol1*12
        cell = np.array([a,b,c,alpha,beta,gamma], dtype = float)

	print "2) Generating initial configuration with ", nmol1, "molecules"
        U_cart = gen_init(1,nmol1,a,b,c,alpha,beta,gamma,seed)
#initialise classes
#do a bunch of rotational and translation moves

        nsteps = 2
        rot_flag = 1 #=> rotations, translations, orthorhombic PBC's
                     #more steps. lot more molecules. min. image conventions
        rot_flag = 2 #=> floppy box=> rot+trans+scaling of box
                     #more steps, nmol <=8,virial calculated.
        rot_flag = 4 #'move+BFGS'
        print "rot_flag =", rot_flag
        if rot_flag == 1:
            print "3) Rotate and translate system within cell, nsteps =", nsteps
            run_moves(nsteps,U_cart,nmol1,cell,rcut)
            print "Steps accepted - rot", count_rot
            print "Steps accepted - trans", count_trans
        elif rot_flag ==2:
            print "3) Floppy box model initiated"
            mv = MV()
            run_moves(nsteps,U_cart,nmol1,cell,rcut)
        elif rot_flag ==3:
            print "3) Testing BFGS with LJ"
        elif rot_flag == 4:
            print "4)NPT - variable cell mode:"
            fl=floppy_box()
            mc = misc()
            U_rel = fl.cell_vectors(U_cart,cell)
            E_new = 0
            mc.write_xyz('flbox.xyz',U_rel,nmol1,name_m,cell,E_new)

            print U_rel



        print "4) Print output in xyz"
#        mc = misc()

main()
