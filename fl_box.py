import numpy as np
import time
from misc import misc
import random
from start_coords import get_init
from  potentials import LJ
from moves import MV

class floppy_box(object):
    # Find numbers N1, N2, N3
    #NPT ensemble, compress to denser systems. set multipple compresssion paths.
    def count_cells(self, U, R0, l1, l2, l3):
        #returns number of cells to be counted in each direction.
        #consider 8 vertices of cube cn
        natom = len(U)
        cn = 2*R0 * np.matrix([
                                    [1, 1, 1],
                                    [-1, 1, 1],
                                    [1, -1, 1],
                                    [1, 1, -1],
                                    [-1, -1, 1],
                                    [1, -1, -1],
                                    [-1, 1, -1],
                                    [-1, -1, -1]
                                ], dtype=float)
        c0 = np.array(cn[0])
        c1 = np.array(cn[1])
        c2 = np.array(cn[2])
        c3 = np.array(cn[3])
        c4 = np.array(cn[4])
        c5 = np.array(cn[5])
        c6 = np.array(cn[6])
        c7 = np.array(cn[7])



        #this cube envelopes the sphere at origin with radius 2R0.
        #here the box vectors are in columns(NOTE)
        M = np.matrix([
            [l1[0], l2[0], l3[0]],
            [l1[1], l2[1], l3[1]],
            [l1[2], l2[2], l3[2]]
        ])
        #invert matrix
        try:
            Minv = np.linalg.inv(M)
        except np.linalg.LinAlgError:
            print "Can't invert cell vector matrix. STOP."
            exit()


        #8 vertices of parallelepiped.

        p0 = Minv*c0.T
        p1 = Minv*c1.T
        p2 = Minv*c2.T
        p3 = Minv*c3.T
        p4 = Minv*c4.T
        p5 = Minv*c5.T
        p6 = Minv*c6.T
        p7 = Minv*c7.T

        #x = np.array(U[:,0],dtype=float)

        #print "Minv", Minv.shape
        #print "cn",c0.shape

        #x = np.reshape(U[:, 0], [natom, 1])
        #y = np.reshape(U[:, 1], [natom, 1])
        #z = np.reshape(U[:, 2], [natom, 1])

        #print "x", x[11],len(U)

        p = np.reshape(np.array([p0, p1, p2, p3, p4, p5, p6, p7], dtype=float), [8, 3])
        #print p



        #apply inverse to 8 vertices of the cube.
        N1 = []
        N2 = []
        N3 = []
        for j in range(natom):
            for i in range(8):

               #calculate max dotproduct N1 = max(pn,x), N2 = max(pn,y), N3=max(pn,z)
                N1.append(p[i, 0])
                N2.append(p[i, 1])
                N3.append(p[i, 2])

        N1 = np.ceil(np.amax(N1))
        N2 = np.ceil(np.amax(N2))
        N3 = np.ceil(np.amax(N3))
        #return number of cells in each direction.
        #print N1


        #print N1,N2,N3
        #N1 = 1
        #2 = 1
        #N3 = 1

        N_cell = np.array([N1, N2, N3], dtype=int)
        #N_cell = [2,2,2]
        print N_cell
        return N_cell

    def make_image(self, U_rel, N1, N2, N3, l1, l2, l3, R0):
        global natom_list,oned_Pim, E_init, E
        #print "*******make image********"
        #given the number of boxes in each direction, returns image list.
        #See "Determining nearest image in non-orthogonal periodic systems", Mihaly Mezei.
        H = np.matrix([
            [l1[0], l2[0], l3[0]],
            [l1[1], l2[1], l3[1]],
            [l1[2], l2[2], l3[2]]
        ])
        #print "Cell vectors: \n ", H.T
        natoms = len(U_rel)
        Hinv = np.linalg.inv(H)
        natom_mol = 2
        nmol = len(U_rel)/natom_mol
        #print Hinv
        #U_frac = Hinv*U_cart
        Pim = []
        oned_Pim = []
        #mv = MV()
        #reciprocal cell vectors
        #scaled coordinates of one point at 0 COM
        s = U_rel * Hinv
        np.where(s > 1)
        #print s
        #print "Real N1, N2, N3 =", N1, N2, N3
        #print "Number of boxes =", 27*(N1*N2*N3)
        #sum over images in all directions. +ve z only
        #sum over imnages in positive
        count = 0
        for i1 in range(-N1+1, N1 + 1):
            for i2 in range(-N2+1, N2 + 1):
                for i3 in range(-N3+1, N3 + 1):
                    count = count+1
                    #construct list of images
                    sum = np.array(U_rel + np.dot(H, [i1, i2, i3]))
                    #sum is always an array of length natom_mol
                    Pim.append(np.array(sum))
        nbox = count
        Pim = np.array(Pim,dtype = float)
        natom_list = nbox*natoms
        #print "Pim length = ", len(Pim)
        #primary stores unit cell coordinates of whichever image you like.
        Pim = np.reshape(Pim,[nbox*natoms,3])
        oned_Pim = np.reshape(Pim,[nbox*natoms*3,1])

        #Pim contains list of all images in neigbhbourhood and primary cell.

        print "No. of atoms in image list", len(Pim)


        #self.get_energyenergy(Pim,R0,l1,l2,l3)

        return np.array(Pim)

    def get_energy(self,Pim,R0,l1,l2,l3,nmol):
        get = get_init()
        commons = get.readmol("dimer.dat")
        natom_mol = commons[0]
        natoms = natom_mol*nmol
        name_m = commons[2]
        charge_m = commons[7]
        cell = [l1,l2,l3]
        E_new = 0.00
        mc = misc()
        #print name_m
        natom_list = len(Pim)
        count = 0
        lj = LJ()
        Epot  = 0
        #print Pim[200]
        #E = lj.amber(Pim,name_m,12)
        for i in xrange(natom_list):
            for j in xrange(i+1,natom_list):
                    d = np.linalg.norm(Pim[i] - Pim[j])
                    atomi = name_m[i % natom_mol]
                    atomj = name_m[j % natom_mol]
                    qi = charge_m[i % natom_mol]
                    qj = charge_m[j % natom_mol]
                    if atomi == 'C':
                        sig1 = 1
                        eps1 = 1

                    if atomi == 'O' :
                        sig2 = 1
                        eps2 = 1
                    if d < 0.005:
                        print "Error:mindist exceeded"
                        exit()
                    if d < R0  :
                        sig = 1
                        eps = 1
                        r6i = (sig/np.array(d))**6
                        Elj = np.sum(4.*eps*(r6i*(r6i -1)))
                        #Elj = lj.nb_williams_83(d,A1,B1,C1,A2,B2,C2)

                        #Ecoul = qi*qj/d
                        Epot = Epot + Elj #+ Ecoul
                        #print "Enerpot", Epot

                        #           print atomi,atomj,d
                        count = count + 1

        #print "Number of relevant interactions =", count


        E = Epot
        print "Energy = ", E/natom_list
        return E

    def cell_vectors(self, U, l1,l2,l3,R0):

        natoms_m = 1  #add read
        natom = len(U)
        nmol = natom / natoms_m
        mc = misc()
        #U_rel = []
        #'find center of rotation for each particle'
        U_com = np.array(mc.COM_all(U, natoms_m))
        #print U_com
        U_com = np.reshape(U_com, [nmol, 3])
        x1 = U_com[0]
        #print "box centered at first particle", x1
        #print ">> one rotation center at origin"
        U_rel = np.subtract(U, x1)
        #       print U_rel
        #volume of one carbon atom ~ 10**-24 cm3 ~ 10 Angstrom3
        vatom = 10
        vol = np.dot(l1, np.cross(l2,l3))
        #determine
        #pack = mc.pack_frac(vatom, vol, natom)
        #if pack > 0.9 :
            #print "WARNING: Packing fraction too high."
        #    exit()


        #each particle has outscribed sphere of radius R0,j
        #n1 = [3,3,3]  #n1[n1,n2,n3] describes number of cells in each direction
        R0 = 2.5
        N_cell = self.count_cells(U_rel, R0, l1, l2, l3)
        print "Cells in each direction [Nx,Ny,Nz]: ", N_cell
        N1 = N_cell[0]
        N2 = N_cell[1]
        N3 = N_cell[2]

        #construct initial image list and calculate energy.

        Pim = self.make_image(U_rel, N1, N2, N3, l1, l2, l3, R0)
        #minimize energy and coordinates
        natom_list = len(Pim)
        return Pim

    def run_moves(self,U,nmol,l1,l2,l3,R0,nsteps):
        mv = MV()
        fb = floppy_box()
        mc = misc()
        get=get_init()
        filename = "dimer.dat"
        commons = get.readmol(filename)
        name_m = commons[2]
        natom_mol = commons[0]
        natoms = natom_mol*nmol

        #initial energy calculation
        #Pim = self.cell_vectors(U, l1,l2,l3,R0)
        #E_init = self.get_energy(Pim,R0,l1,l2,l3,nmol)
        H = np.matrix([
                          [l1[0], l2[0], l3[0]],
                          [l1[1], l2[1], l3[1]],
                          [l1[2], l2[2], l3[2]]
                      ],dtype =float)
        cell_old = H.T
        #STRATEGY IS AS FOLLOWS:
        #1)'CELL INPUT: DO NSTEPS ROTATION'
        #2) REDUCE CELL:
        #change cell vector:

        f = open ("energies.dat",'a',0)
        f.write('Energy\tcellstep\trotstep\tvolume\n')
        temp = 1
        rand = random.random()
        nsteps_cell = 1
        nsteps_rot = nsteps
        step = 0.0
        start_time = time.time()
        step_t = l1[0] * 0.1
        #count cells and make image
        step_cell = 0.1
        Pim_new = self.cell_vectors(U, l1, l2, l3, R0)
        natomlist = len(Pim_new)
        #print"Number of atoms in list =", len(Pim_new)
        #get     energy
        E_init = self.get_energy(Pim_new, R0, l1, l2, l3, nmol)
        cell_vec = [l1, l2, l3]
        #mc.write_xyz('image.xyz',Pim_new,nmol,name_m,cell_vec,E_init)

        count_rot_a = 0
        count_rot_r = 0
        U_old = U
        E_old = E_init

        constant_vol = False
        for j in xrange(nsteps_rot):
                    #decide step probability
                    print "nstep = ", j
                    o = np.random.uniform(0,1)
                    if (o < 0.4):
                        print "ROTATE", o

                        U_new = mv.rotate_mol_euler(U_old,nmol)
                        cell_new = cell_old
                        cell_init = cell_old
                        l1[0] = cell_new[0, 0]
                        l1[1] = cell_new[0, 1]
                        l1[2] = cell_new[0, 2]
                        l2[0] = cell_new[1, 0]
                        l2[1] = cell_new[1, 1]
                        l2[2] = cell_new[1, 2]
                        l3[0] = cell_new[2, 0]
                        l3[1] = cell_new[2, 1]
                        l3[2] = cell_new[2, 2]
                    elif (o < 0.9) and (o>0.4) :
                        print "TRANSLATE"
                        translate = True
                        U_new = mv.translate_mol(U_old,nmol,step_t)
                        cell_new = cell_old
                        l1[0] = cell_new[0, 0]
                        l1[1] = cell_new[0, 1]
                        l1[2] = cell_new[0, 2]
                        l2[0] = cell_new[1, 0]
                        l2[1] = cell_new[1, 1]
                        l2[2] = cell_new[1, 2]
                        l3[0] = cell_new[2, 0]
                        l3[1] = cell_new[2, 1]
                        l3[2] = cell_new[2, 2]

                    elif (o > 0.9) :
                        print "SCALE BOX"
                        box_move = True
                        U_new = U
                        cell_new = mv.scale_box_new(cell_old, step_cell)
                        l1[0] = cell_new[0, 0]
                        l1[1] = cell_new[0, 1]
                        l1[2] = cell_new[0, 2]
                        l2[0] = cell_new[1, 0]
                        l2[1] = cell_new[1, 1]
                        l2[2] = cell_new[1, 2]
                        l3[0] = cell_new[2, 0]
                        l3[1] = cell_new[2, 1]
                        l3[2] = cell_new[2, 2]
                        if l1[0] < 1 or l2[1] < 1 or l3[2] <1 :
                            print "STOP:Box out of shape"
                            #'set new seed'
                            cell_new = cell_init
                            l1[0] = cell_new[0, 0]
                            l2[1] = cell_new[0, 1]
                            l3[2] = cell_new[2, 2]
                            U_new = get.gen_init(1, nmol, l1[0] ,l2[1],l3[2],90, 90, 90, seed=10000)
                            print cell_new

                    #volume for each step.
                    cross = np.cross(l2, l3)
                    vol = np.dot(l1, cross)  #in angstrom3
                    print "VOLUME = ", vol

                    step_t = l1[0] * 0.1
                    #calculate images
                    Pim = fb.cell_vectors(U, l1,l2,l3,R0)
                    #get energy
                    E_new = self.get_energy(Pim,R0,l1,l2,l3,R0)
                    #print "energy =", E_new
                    accept = mc.accept_reject(E_new,E_old,temp,rand)
                    if accept:
                        f.write('%9.3f\t%d\t%0.3f %d %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f\n'%(E_old/natomlist,j,vol,natomlist,l1[0],l1[1],l1[2],l2[0],l2[1],l2[2],l3[0],l3[1],l3[2]))
                        mc.write_xyz('unit_cell.xyz',U_new,vol,name_m,cell_vec,E_old/natomlist)
                        mc.write_xyz('image.xyz',Pim,vol,name_m,cell_vec,E_old/natomlist)
                        f.flush()
                        count_rot_a=count_rot_a+1
                        #update U, E and cell
                        U_old = U_new
                        E_old = E_new
                        count_rot_a  = count_rot_a+1
                        cell_old = cell_new
                        count_rot_r=count_rot_r+1
                    if not accept:
                        cell_new = cell_old
                        U_new = U_old

        rot_time = time.time()
        print "rot_time = ", rot_time - start_time
        #write lowest energy structure found for each volume.
        mc.write_xyz('final_image.xyz',Pim,nmol,name_m,cell_vec,E_old/natomlist)
        #U_primary= Pim[:natoms]
        mc.write_xyz('unit_cell.xyz',U,nmol,name_m,cell_vec,E_old)
        f.close()


        return
    def run_moves_constant_volume(self,U,nmol,l1,l2,l3,R0,nsteps):
        mv = MV()
        fb = floppy_box()
        mc = misc()
        get=get_init()
        filename = "dimer.dat"
        commons = get.readmol(filename)
        name_m = commons[2]
        natom_mol = commons[0]
        natoms = natom_mol*nmol

        #initial energy calculation
        #Pim = self.cell_vectors(U, l1,l2,l3,R0)
        #E_init = self.get_energy(Pim,R0,l1,l2,l3,nmol)
        H = np.matrix([
                          [l1[0], l2[0], l3[0]],
                          [l1[1], l2[1], l3[1]],
                          [l1[2], l2[2], l3[2]]
                      ],dtype =float)
        cell_old = H.T
        #STRATEGY IS AS FOLLOWS:
        #1)'CELL INPUT: DO NSTEPS ROTATION'
        #2) REDUCE CELL:
        #change cell vector:

        f = open ("energies.dat",'a',0)
        f.write('Energy\tcellstep\trotstep\tvolume\n')
        temp = 1
        rand = random.random()
        nsteps_cell = 1
        nsteps_rot = nsteps
        step = 0.1
        start_time = time.time()
        step_t = 0.1
        #count cells and make image
        step_cell = 0.1
        Pim_new = self.cell_vectors(U, l1, l2, l3, R0)
        natomlist = len(Pim_new)
        #print"Number of atoms in list =", len(Pim_new)
        #get     energy
        E_init = self.get_energy(Pim_new, R0, l1, l2, l3, nmol)
        cell_vec = [l1, l2, l3]
        #mc.write_xyz('image.xyz',Pim_new,nmol,name_m,cell_vec,E_init)

        count_rot_a = 0
        count_rot_r = 0
        U_old = U
        E_old = E_init

        constant_vol = False
        for j in xrange(nsteps_rot):
                    #decide step probability
                    print "nstep = ", j
                    o = np.random.uniform(0,1)
                    if (o < 0.5):
                        print "ROTATE", o

                        U_new = mv.rotate_mol_euler(U_old,nmol)
                        cell_new = cell_old
                        cell_init = cell_old
                        l1[0] = cell_new[0, 0]
                        l1[1] = cell_new[0, 1]
                        l1[2] = cell_new[0, 2]
                        l2[0] = cell_new[1, 0]
                        l2[1] = cell_new[1, 1]
                        l2[2] = cell_new[1, 2]
                        l3[0] = cell_new[2, 0]
                        l3[1] = cell_new[2, 1]
                        l3[2] = cell_new[2, 2]
                    elif (o < 1) and (o>0.5) :
                        print "TRANSLATE"
                        translate = True
                        U_new = mv.translate_mol(U_old,nmol,step_t)
                        cell_new = cell_old
                        l1[0] = cell_new[0, 0]
                        l1[1] = cell_new[0, 1]
                        l1[2] = cell_new[0, 2]
                        l2[0] = cell_new[1, 0]
                        l2[1] = cell_new[1, 1]
                        l2[2] = cell_new[1, 2]
                        l3[0] = cell_new[2, 0]
                        l3[1] = cell_new[2, 1]
                        l3[2] = cell_new[2, 2]
                    #volume for each step.
                    cross = np.cross(l2, l3)
                    vol = np.dot(l1, cross)  #in angstrom3
                    print "VOLUME = ", vol

                    step_t = l1[0] * 0.1
                    #calculate images
                    Pim = fb.cell_vectors(U, l1,l2,l3,R0)
                    #get energy
                    E_new = self.get_energy(Pim,R0,l1,l2,l3,R0)
                    #print "energy =", E_new
                    accept = mc.accept_reject(E_new,E_old,temp,rand)
                    if accept:
                        f.write('%9.3f\t%d\t%0.3f %d %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f %0.3f\n'%(E_old/natomlist,j,vol,natomlist,l1[0],l1[1],l1[2],l2[0],l2[1],l2[2],l3[0],l3[1],l3[2]))
                        mc.write_xyz('unit_cell.xyz',U_new,vol,name_m,cell_vec,E_old/natomlist)
                        mc.write_xyz('image.xyz',Pim,vol,name_m,cell_vec,E_old/natomlist)
                        f.flush()
                        count_rot_a=count_rot_a+1
                        #update U, E and cell
                        U_old = U_new
                        E_old = E_new
                        count_rot_a  = count_rot_a+1
                        cell_old = cell_new
                        count_rot_r=count_rot_r+1
                    if not accept:
                        cell_new = cell_old
                        U_new = U_old

        rot_time = time.time()
        print "rot_time = ", rot_time - start_time
        #write lowest energy structure found for each volume.
        mc.write_xyz('final_image.xyz',Pim,nmol,name_m,cell_vec,E_old/natomlist)
        #U_primary= Pim[:natoms]
        mc.write_xyz('unit_cell.xyz',U,nmol,name_m,cell_vec,E_old)
        f.close()


        return


def main():
    # strategy for optimization here.

    seed = [10000,10001,10002,10003,10004,1005,1006,1007,10008,1009]
    get = get_init()
    l1 = [3 , 0, 0]
    l2 = [0, 3, 0]
    l3 = [0, 0, 3 ]
    nmol = 4
    R0 = 2.5
    nsteps = 20000
    #commons=[natom_m,type_m,name_m,U_m,mass_m,ntype,atype,charge_m]
    commons = get.readmol("dimer.dat")
    #commons = get.readmol("carbon_atom.dat")
    natoms = commons[0]
    # 1) GENERATE INITIAL CONFIGURATION
    #for seed in seed1,seed2,seed3,seed4,seed5 :

    #print "SET SEED =",seed[]
    mc = misc()
    mc.clean_files()
    fb = floppy_box()
    constant_volume=False

    if constant_volume:
        for i in range(1,10):
            U = get.gen_init(1, nmol, 2.0 ,4.0 ,4.0,90, 90, 90, seed[i])
            fb.run_moves_constant_volume(U,nmol,l1,l2,l3,R0,nsteps)
    else:

        for i in range(1,10):
             print "Scale moves allowed"
             U = get.gen_init(1, nmol, 2.0 ,4.0 ,4.0,90, 90, 90, seed[i])
            #rotate/translate/movebox construct list and calculate energy
            fb.run_moves(U,nmol,l1,l2,l3,R0,nsteps)

    #minimize
    #E = lj.bm(U,cell,atomnames,rcut,charge)
    #print "Ener", E
main()
