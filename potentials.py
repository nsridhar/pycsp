import numpy as np
from numpy import sqrt
from numpy import atleast_1d, eye, mgrid, argmin, zeros, shape, \
     squeeze, vectorize, asarray, sqrt, Inf, asfarray, isinf

import math



# Energy  = Eintra + Evdw + Eelec + Eother
class LJ() :
# read eps and sig from file
    def __init__(self, eps = 0.001, sig = 3.0, rcut = 10):
        self.sig = sig
        self.eps = eps
        self.rcut = rcut*self.sig

    def lj(self,x) :
    #x:1-D array of 3N points at which LJ function is to be computed.
        x = asarray(x)
        eps = 1 #epsilon/Kb
        sig = 1
        r6i = (sig/np.array(x))**6

        return np.sum(4.*eps*(r6i*(r6i -1)))


    def lj_der(x) :
        x = asarray(x)
        eps = 1
        sig = 1
        r = asarray(x)
        r2i = (sig/r)**2
        r6i = r2i*r2i*r2i
        der = (4.*eps*( -12./sig*(sig/r)**13 + 6./sig*(sig/r)**7 ))
        return der


#   potential
    def vij(self, r):
        return 4. * self.eps * ( (self.sig / r) ** 12 - (self.sig / r) ** 6 )
#   derivative
    def dvij(self, r):
        return 4. * self.eps * ( -12. / self.sig * (self.sig / r) ** 13 + 6. / self.sig * (self.sig / r) ** 7 )

    def vij_bm(self,A,B,C,r):
        A = A*0.04336340
        B = B*0.0433634
        C = C
        r6i = r**(-6)
        return np.float64(A*r6i+B*math.exp(-C*r))
    def dvij_bm(self,A,B,C,r):
        A = A*0.04336340
        B = B*0.0433634
        C = C
        r7i = r**(-7)
        return -6*A*r7i - B*C*math.exp(-C*r)

    def v_coulomb(self,q1,q2,r,alpha,r_cut):
        multipl_const=14.38008*q1*q2
        alpha_r = alpha/r
        alpha_rcut = alpha/r_cut
        erfc_alpha_rcut=1.0-math.erf(alpha_rcut)
        erfc_alpha_r = math.erfc(alpha_r)
        erfc_alpha_rcut = math.erfc(alpha_rcut)
        exp_minus_alpha_rcut_sq=math.exp(-alpha_rcut*alpha_rcut)
        one_over_r = 1.0/r
        one_over_rcut = 1.0/r_cut
        two_alpha_over_sqrt_pi = 2.0*alpha/1.77245385
        v_coul=   multipl_const * (erfc_alpha_r*one_over_r - erfc_alpha_rcut*one_over_rcut
      + (r-r_cut) *      (erfc_alpha_rcut*one_over_rcut*one_over_rcut
       + two_alpha_over_sqrt_pi*exp_minus_alpha_rcut_sq
       *one_over_rcut) )

        pot_deriv_times_dist=  (multipl_const * ( - erfc_alpha_r*one_over_r
        - two_alpha_over_sqrt_pi*math.exp(-alpha_r*alpha_r) + erfc_alpha_rcut*one_over_rcut*one_over_rcut*r
        + two_alpha_over_sqrt_pi*exp_minus_alpha_rcut_sq
        *one_over_rcut*r ))/r;

        return v_coul
    def dv_coulomb(self,q1,q2,e,alpha,r_cut):
        multipl_const=14.38008*q1*q2
        alpha_r = alpha/r
        alpha_rcut = alpha/r_cut
        erfc_alpha_rcut=1.0-math.erf(alpha_rcut)
        erfc_alpha_r = math.erfc(alpha_r)
        erfc_alpha_rcut = math.erfc(alpha_rcut)
        exp_minus_alpha_rcut_sq=math.exp(-alpha_rcut*alpha_rcut)
        one_over_r = 1.0/r
        one_over_rcut = 1.0/r_cut
        two_alpha_over_sqrt_pi = 2.0*alpha/1.77245385

        return (multipl_const * ( - erfc_alpha_r*one_over_r
        - two_alpha_over_sqrt_pi*math.exp(-alpha_r*alpha_r) + erfc_alpha_rcut*one_over_rcut*one_over_rcut*r
        + two_alpha_over_sqrt_pi*exp_minus_alpha_rcut_sq
        *one_over_rcut*r ))/r;

    def nb_williams_83(self,r,A1,B1,C1,A2,B2,C2):
        #non-bonded williams potential for hydrocarbons
        r6i = 1/r**6
        E12 = B1*B2*np.exp(-(C1+C2)*r) - (A1*A2)*r6i
        return E12

    def bm(self,U,cell,atomlist,rc,charge):
        #print "in bm"
        #compute distancea
        #apply PBCs in 3 dimensions
        ndim = 3
        npart = len(U)
        fracflag = 1
        #print "number of atoms =", npart
        a = cell[0]
        b = cell[1]
        c = cell[2]
        r = []
        Epot = np.float64(0.0)
        #print "Epot_start", Epot
        #Emolecule = 0! only intermolecular interactions man.??
        count = 0
        for i in range(npart):
            for j in range(i,npart):
                if i != j:
                    X  = U[j,0] - U[i,0]
                    Y  = U[j,1] - U[i,1]
                    Z  = U[j,2] - U[i,2]
                    # Periodic boundary conditions(orthorhmobic-cell)
                    X  -= a*np.rint(X/a)
                    Y  -= b*np.rint(Y/b)
                    Z  -= c*np.rint(Z/c)

                    # Distance squared
                    r2 = X*X + Y*Y + Z*Z
                    r = sqrt(r2)
                    #print r
                    #store in list
                    r6 = r2*r2*r2

                    #r.append([r2])

                    #assign interaction parameters.
                    'http://pubs.acs.org/doi/pdf/10.1021/j150665a034'
                    #print atomlist[i],atomlist[j]
                    if atomlist[i] == 'C' and atomlist[j] == 'H' :
                        count = count+1
                        Aij = 125.0
                        Bij = 8766
                        Cij = 3.67
                    if atomlist[i] == 'C' and atomlist[j] == 'C' :
                        count = count +1
                        Aij = 568.0
                        Bij = 83630
                        Cij = 27.3
                    if atomlist[i] == 'H' and atomlist[j] == 'H' :
                        count = count +1
                        Aij = 27.3
                        Bij = 2654.0
                        Cij = 3.74
                    #                   print atomlist[i],atomlist[j],Aij,Bij,Cij
                    r2i = 1/r2
                    r6i = 1/r6
                    rc2i = 1/rc*rc
                    rc6i=rc2i*rc2i*rc2i

                    ecut=rc6i*(rc6i-1.0)
                    q1 = charge[i]
                    q2 = charge[j]
                    alpha = 0.4
                    #print q1,q2
                    e_coul = np.float64(self.v_coulomb(q1,q2,r,alpha,rc))
                    e_bm=np.float64(self.vij_bm(Aij,Bij,Cij,r))


                    #print Cij,r

                    if r < rc and r>0.5:
                        #print Epot
                        Epot = Epot + e_bm + e_coul

                        if Epot > 10**10 or Epot<-10**10:
                            print "energy too large or too small"
                            quit()

                                                #print Epot

        #print count
        return np.float64(Epot)

    def lennardjones(self,U,cell,rc2):
        #cut-off
        rc2i=1./rc2
        rc6i=rc2i*rc2i*rc2i
        rm = self.sig**(1/6)
        ecut=rc6i*(rc6i-1.0)
        mindist = 0.5

    # Can't use (ndim, npart) = numpy.shape(U)
    # with Numba. No unpacking of tuples.
        ndim = 3
        npart = len(U)
        #print npart
        #Initialize forces
        F = np.zeros((npart,ndim))
        #print U
        Epot = 0.0
        Vir = 0.0
        rc2 = 100
        a = cell[0]
        b = cell[1]
        c = cell[2]
        #print "Epot_start", Epot
        for i in range(npart):
            for j in range(npart):
                if i > j:
                    X  = U[j,0] - U[i,0]
                    Y  = U[j,1] - U[i,1]
                    Z  = U[j,2] - U[i,2]
                    # Periodic boundary condition
                    X  -= a*np.rint(X/a)
                    Y  -= b*np.rint(Y/b)
                    Z  -= c*np.rint(Z/c)

                    rij = np.array(X + Y + Z)
                    # Distance squared
                    r2 = X*X + Y*Y + Z*Z
                    r = sqrt(r2)


                    if(r < rc2) and (r>mindist):
                        r2i = 3.4*3.4 / r2
                        r6i = r2i*r2i*r2i

                        Epot = Epot + 4.0*r6i*(r6i-1) - ecut

                        #print Epot,i,j,r


                        ftmp = 48.*r6i*(r6i- 0.5) * r2i

                        F[i, 0] -= ftmp * X
                        F[i, 1] -= ftmp * Y
                        F[i, 2] -= ftmp * Z
                        F[j, 0] += ftmp * X
                        F[j, 1] += ftmp * Y
                        F[j, 2] += ftmp * Z
                        Vir += ftmp
        #print "E_pot = ", Epot, "Virial = ", Vir
        return Epot

    def amber(self,U,atomname,natom_m):
       natom_m = 12
       f = open ("amber.pdb", 'w')
       x = 'ATOM  '
       natom = len(U)
       residue = 'BENZ'

       occ = 1.00
       temp = 0.00
       for i in range (natom) :
           res_id = np.floor(i/natom_m)+ 1
           atom= atomname[i % natom_m]

           f.write('%6s %4i %2s   %s   %2i    %8.3f%8.3f%8.3f%6.2f%6.2f  \n'%(x,i+1,atom,residue,res_id,U[i,0],U[i,1],U[i,2],occ,temp))






def main():  # pragma: no cover
    natoms = 12
    seed = 12345
    U = np.random.uniform(-10,10, natoms * 3)
    U = np.reshape(U, [natoms, 3])
    lj = LJ()
    a = 25
    b = 25
    c = 25
    rcut=7
    alpha = 90
    beta = 90
    gamma = 90
    moltype=1
    cell = [a,b,c,alpha,beta,gamma]



    l1 = [10, 0, 0]
    l2 = [0, 10 ,0]
    l3 = [0.,0,10]
    atomlist = ['C', 'C', 'C', 'C', 'C','C', 'H', 'H', 'H', 'H', 'H', 'H', 'C', 'C', 'C', 'C', 'C', 'C', 'H', 'H', 'H', 'H', 'H', 'H', 'C', 'C', 'C', 'C', 'C', 'C', 'H', 'H', 'H', 'H', 'H', 'H']
    charge = [0.1,0.1,0.1,0.1,0.1,-0.1,-0.1,-0.1,-0.1,-0.1]
   # E = lj.lennardjones(U,cell,10)
   # E= lj.bm(U,cell,atomlist,10,charge)
    E = lj.amber(U,atomlist,12)


    print "E", E



if __name__ == "__main__":
    main()
