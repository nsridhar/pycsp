import numpy
from numpy import atleast_1d, eye, mgrid, argmin, zeros, shape, \
     squeeze, vectorize, asarray, sqrt, Inf, asfarray, isinf
from scipy.optimize import minimize
from start_coords import get_init
from misc import misc



def rosen(x):
    """
    The Rosenbrock function.

    The function computed is::

        sum(100.0*(x[1:] - x[:-1]**2.0)**2.0 + (1 - x[:-1])**2.0

    Parameters
    ----------
    x : array_like
        1-D array of points at which the Rosenbrock function is to be computed.

    Returns
    -------
    f : float
        The value of the Rosenbrock function.

    See Also
    --------
    rosen_der, rosen_hess, rosen_hess_prod

    """
    x = asarray(x)
    return numpy.sum(100.0*(x[1:] - x[:-1]**2.0)**2.0 + (1 - x[:-1])**2.0, axis=0)

def rosen_der(x):
    """
    The derivative (i.e. gradient) of the Rosenbrock function.

    Parameters
    ----------
    x : array_like
        1-D array of points at which the derivative is to be computed.

    Returns
    -------
    rosen_der : (N,) ndarray
        The gradient of the Rosenbrock function at `x`.

    See Also
    --------
    rosen, rosen_hess, rosen_hess_prod

    """
    x = asarray(x)
    xm = x[1:-1]
    xm_m1 = x[:-2]
    xm_p1 = x[2:]
    der = numpy.zeros_like(x)
    der[1:-1] = 200*(xm - xm_m1**2) - 400*(xm_p1 - xm**2)*xm - 2*(1 - xm)
    der[0] = -400*x[0]*(x[1] - x[0]**2) - 2*(1 - x[0])
    der[-1] = 200*(x[-1] - x[-2]**2)
    return der

def lj(x) :
    #x:1-D array of 3N points at which LJ function is to be computed.
    x = asarray(x)
    eps = 1 #epsilon/Kb
    sig = 1
    r6i = (sig/numpy.array(x))**6

    return numpy.sum(4.*eps*(r6i*(r6i -1)))

def lj_der(x) :
    x = asarray(x)
    eps = 1
    sig = 1
    r = asarray(x)
    r2i = (sig/r)**2
    r6i = r2i*r2i*r2i

    der = (4.*eps*( -12./sig*(sig/r)**13 + 6./sig*(sig/r)**7 ))
    return der

def vij_bm(x):
        x = asarray(x)

        A = 125*0.04336340
        B = 8766*0.0433634
        C = 3.67
        r6i = (r)**(-6)
        Asum= A*numpy.sum(r6i)
        Bsum = B*numpy.sum(numpy.exp(-C*numpy.array(x)))
        pot = Asum - Bsum
        return pot

def dvij_bm(x):
        x = asarray(x)
        A = 125*0.04336340
        B = 8766*0.0433634
        C = 3.67
        r7i = numpy.array(x)**(-7)
        q1 = 0.2*1.6*10**(-19)
        q2 = -0.2*1.6*10**(-19)
        return -6*A*r7i - B*C*numpy.exp(-C*numpy.array(x))


def main():
        #get initial coordinates

        seed = None
        get = get_init()
        nmol = 20
        #commons=[natom_m,type_m,name_m,U_m,mass_m,ntype,atype,charge_m]
        commons = get.readmol("dimer.dat")
        #commons = get.readmol("carbon_atom.dat")
        # 1) GENERATE INITIAL CONFIGURATION
        U0 = get.gen_init(1, nmol, 8, 8, 8, 90, 90, 90, seed)

        natom = len(U0)
        print "natom = ", natom
        numpy.random.seed(12341)

        r = asarray(U0)
        r = numpy.reshape(r,[natom,3])
        rij = []
        for i in range(natom):
            for j in range(i+1,natom):
                rij = r[i] - r[j]
        print "ncoords = ", len(U0)
                #U0 = numpy.random.uniform(-1,1,natoms*3)*(1.*natoms)**(1./3)*.1
        #print "Initial", U0
        pot = 'LJ'

        if pot == 'LJ' :
            res = minimize(lj, U0, method='BFGS', jac=lj_der,options={'gtol': 1e-4, 'disp': False})
            #print "Value of minimised function: ", res.fun
            #print "Value of minimized array", res.x
            der = res.x
            der = numpy.reshape(der, [natom,3])
            r_bf = []
            for i in range(natom):
                r_bf.append(asarray(der[i]*r[i]))
        U_bfgs = numpy.reshape(r_bf,[natom,3])
        print U_bfgs
        mc = misc()
        name_m = commons[2]
        print name_m
        cell = []
        E = 0.0000
        mc.write_xyz('bfgs.xyz',U_bfgs,20,name_m,cell,E)


        success =  res.success
        if success:
            print "SUCCESS:Energy minimized to", res.fun, "eV"

            #print "Final", res.x
            #print "iterations", res


        else :
            print "FAIL: Energy evaluation in bfgs failed, check gtol"

#x	(ndarray) The solution of the optimization.
#success	(bool) Whether or not the optimizer exited successfully.
#status	(int) Termination status of the optimizer. Its value depends on the underlying solver. Refer to message for details.
#message	(str) Description of the cause of the termination.
#fun, jac, hess, hess_inv	(ndarray) Values of objective function, Jacobian, Hessian or its inverse (if available). The Hessians may be approximations, see the documentation of the function in question.
#nfev, njev, nhev	(int) Number of evaluations of the objective functions and of its Jacobian and Hessian.
#nit	(int) Number of iterations performed by the optimizer.
#maxcv	(float) The maximum constraint violation''



#        print res



main()


